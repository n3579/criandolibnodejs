const fetch = require("node-fetch");

function manejaErros(erro){
    throw new Error(erro.message);
}

async function checaStatus(arrayURLs){

    try {
        const arrayStatus = await Promise
            .all(arrayURLs
                .map(async url => {
                    const res = await fetch(url);
                    return res.status;
                })
            )
        return arrayStatus;  
             
    } catch (erro) {
        manejaErros(erro);        
    }
    
}

function geraArrayDeUrls(arrayLinks) {
    
    return arrayLinks
        .map(objetoLinks => Object
            .values(objetoLinks).join())
}

async function validaURLs(arrayDeLinks) {
    const links = geraArrayDeUrls(arrayDeLinks);

    const statusLinks = await checaStatus(links);

    const resultados = arrayDeLinks.map((objeto, indice) => ({
        ...objeto, 
        status: statusLinks[indice] 
    }))
    
    return resultados;
}

module.exports = validaURLs;