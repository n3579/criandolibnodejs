const chalk = require('chalk');
const pegaArquivo = require('./index.js');
const validaURLs = require('./http-valicadao.js');

const caminho = process.argv;

async function processaTexto(caminhoDeArquivo) {
    const resultado = await pegaArquivo(caminhoDeArquivo[2]);

    if (caminhoDeArquivo[3] === 'validar') {
        console.log(chalk.yellow('Links Validados'), await validaURLs(resultado));
    } else {
        console.log(chalk.yellow('Lista de Links'), resultado);
    }
   
}

processaTexto(caminho);