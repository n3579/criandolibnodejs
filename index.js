const chalk = require('chalk');
const fs = require('fs');

function extraiLinks(texto) {
    //Utilizado Expressoes Regulares para localizar as Strings desejadas
    const regex = /\[([^\]]*)\]\((https?:\/\/[^$#\s].[^\s]*)\)/gm;

    const arrayResultados = [];

    let temp;

    while ((temp = regex.exec(texto)) != null) {
        arrayResultados.push({
            [temp[1]]: temp[2]
        });                
    }

    return arrayResultados.length === 0 ? 'Não há Links' : arrayResultados;
}

function trataErro(erro) {
    throw new Error(chalk.red(erro.code, 'Não há arquivo no caminho...'));
}

async function pegaArquivo(path) {
    const encoding = 'utf-8'
    try {
        const texto = await fs.promises.readFile(path, encoding)
        return extraiLinks(texto);
    } catch (erro) {
        trataErro(erro);        
    }
}

module.exports = pegaArquivo;